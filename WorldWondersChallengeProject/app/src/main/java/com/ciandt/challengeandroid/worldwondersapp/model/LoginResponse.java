package com.ciandt.challengeandroid.worldwondersapp.model;

/**
 * Login response.
 */
public class LoginResponse {

    private final String mMessage;
    private final UserData mUserData;

    public LoginResponse(String message, UserData userData) {
        mMessage = message;
        mUserData = userData;
    }

    public String getMessage() {
        return mMessage;
    }

    public UserData getUserData() {
        return mUserData;
    }
}
