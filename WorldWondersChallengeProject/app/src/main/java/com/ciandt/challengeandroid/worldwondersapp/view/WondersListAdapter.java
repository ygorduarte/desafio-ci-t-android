package com.ciandt.challengeandroid.worldwondersapp.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.model.Wonder;

import java.util.ArrayList;
import java.util.List;

/**
 * Wonders list recycler view adapter.
 */
public class WondersListAdapter extends RecyclerView.Adapter<WondersListAdapter.ViewHolder> {

    private List<Wonder> mWonders;

    public WondersListAdapter() {
        mWonders = new ArrayList<>();
    }

    public void setWonders(@NonNull List<Wonder> wonders) {
        mWonders = wonders;
        notifyDataSetChanged();
    }

    public void addWonder(Wonder wonder) {
        if (mWonders == null) {
            return;
        }
        mWonders.add(wonder);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (mWonders == null || mWonders.get(position) == null) {
            return;
        }

        Wonder wonder = mWonders.get(position);
        holder.mName.setText(wonder.getName());
        holder.mCountry.setText(wonder.getCountry());
        holder.mDescription.setText(wonder.getDescription());
    }

    @Override
    public int getItemCount() {
        return (mWonders == null) ? 0 : mWonders.size();
    }

    /**
     * View holder.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mName;
        public TextView mCountry;
        public TextView mDescription;

        public ViewHolder(View view) {
            super(view);

            mName = (TextView) view.findViewById(R.id.txt_name);
            mCountry = (TextView) view.findViewById(R.id.txt_country);
            mDescription = (TextView) view.findViewById(R.id.txt_description);
        }
    }
}
