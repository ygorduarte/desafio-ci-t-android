package com.ciandt.challengeandroid.worldwondersapp.model;

/**
 * Wonder data.
 */
public class Wonder {

    private final String mName;
    private final String mCountry;
    private final String mDescription;

    public Wonder(String name, String country, String description) {
        mName = name;
        mCountry = country;
        mDescription = description;
    }

    public String getName() {
        return mName;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getDescription() {
        return mDescription;
    }
}
