package com.ciandt.challengeandroid.worldwondersapp.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Login activity.
 */
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.txt_email)
    EditText txtEmail;

    @BindView(R.id.txt_password)
    EditText txtPassword;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.progress)
    View progressBar;

    private LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mPresenter = new LoginPresenter(this);
        mPresenter.initialize();
    }

    @OnClick(R.id.btn_login)
    public void onLoginClick(View view) {
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();

        mPresenter.onLogin(email, password);
    }

    /**
     * Displays an error message for e-mail input.
     *
     * @param messageResId String resource id for error message.
     */
    public void showEmailError(int messageResId) {
        txtEmail.setError(getString(messageResId));
    }

    /**
     * Displays an error message for password input.
     *
     * @param messageResId String resource id for error message.
     */
    public void showPasswordError(int messageResId) {
        txtPassword.setError(getString(messageResId));
    }

    public void onLoginSuccess(String userName) {
        Toast.makeText(this, getString(R.string.msg_greetings, userName), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void onLoginFail(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void onLoginFail(int stringResource) {
        Toast.makeText(this, stringResource, Toast.LENGTH_SHORT).show();
    }

    public void loadingMode(boolean enabled) {
        progressBar.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
        txtEmail.setEnabled(!enabled);
        txtPassword.setEnabled(!enabled);
        btnLogin.setEnabled(!enabled);
    }
}
