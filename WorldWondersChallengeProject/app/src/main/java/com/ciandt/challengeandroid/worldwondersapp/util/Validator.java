package com.ciandt.challengeandroid.worldwondersapp.util;

import java.util.regex.Pattern;

/**
 * Util class to validate parameters.
 */
public class Validator {

    private static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}"
                    + "\\@"
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}"
                    + "("
                    + "\\."
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}"
                    + ")+"
    );

    public static final int EMPTY_OR_NULL = 0;

    public static final int INVALID = 1;

    public static final int VALID = 2;

    public static int validateEmail(String email) {
        if (email == null || email.isEmpty()) {
            return EMPTY_OR_NULL;
        } else {
            return (EMAIL_ADDRESS_PATTERN.matcher(email).matches()) ? VALID : INVALID;
        }
    }

    public static int validatePassword(String password) {
        return (password == null || password.isEmpty()) ? EMPTY_OR_NULL : VALID;
    }
}
