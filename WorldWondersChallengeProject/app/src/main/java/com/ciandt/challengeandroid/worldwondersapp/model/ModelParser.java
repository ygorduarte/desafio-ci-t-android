package com.ciandt.challengeandroid.worldwondersapp.model;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * JSON parse to create models.
 */
public class ModelParser {

    private static final String TAG = "ModelParser";

    private static final int CODE_SUCCESS = 0;

    public LoginResponse parseLoginResponse(@NonNull String body) {
        LoginResponse response = null;

        try {
            JSONObject jsonBody = new JSONObject(body);
            int code = jsonBody.getInt("codigo");
            String message = jsonBody.getString("mensagem");
            UserData userData = null;
            if (code == CODE_SUCCESS) {
                JSONObject data = jsonBody.getJSONObject("dados");
                String token = data.getString("token_auth");
                String name = data.getString("nome");
                boolean enabled = data.getBoolean("habilitado");
                userData = new UserData(token, name, enabled);
            }
            response = new LoginResponse(message, userData);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

        return response;
    }

    public List<Wonder> parseWondersList(@NonNull String response) {
        ArrayList<Wonder> list = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(response);
            JSONArray data = json.getJSONArray("data");
            Wonder wonder;
            for (int i = 0; i < data.length(); i++) {
                wonder = jsonToWonder(data.getJSONObject(i));
                if (wonder != null) {
                    list.add(wonder);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return list;
    }

    private Wonder jsonToWonder(JSONObject json) {
        Wonder wonder = null;

        try {
            String name = json.getString("name");
            String country = json.getString("country");
            String description = json.getString("description");

            wonder = new Wonder(name, country, description);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

        return wonder;
    }
}
