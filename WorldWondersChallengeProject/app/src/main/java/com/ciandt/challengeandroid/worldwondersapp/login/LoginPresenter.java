package com.ciandt.challengeandroid.worldwondersapp.login;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.model.LoginResponse;
import com.ciandt.challengeandroid.worldwondersapp.model.UserData;
import com.ciandt.challengeandroid.worldwondersapp.service.MockService;
import com.ciandt.challengeandroid.worldwondersapp.util.SharePrefs;
import com.ciandt.challengeandroid.worldwondersapp.util.Validator;

/**
 * Login presenter.
 */
public class LoginPresenter {

    private LoginActivity mActivity;

    private SharePrefs mPrefs;

    /**
     * Constructor.
     *
     * @param activity Login Activity
     */
    public LoginPresenter(@NonNull LoginActivity activity) {
        mActivity = activity;
        mPrefs = SharePrefs.getInstance(activity);
    }

    public void onLogin(String email, String password) {
        if (validate(email, password)) {
            performLogin(email, password);
        }
    }

    public void initialize() {
        if (mPrefs.isLoggedIn()) {
            mActivity.onLoginSuccess(mPrefs.getUserName());
        }
    }

    private void performLogin(String email, String password) {
        mActivity.loadingMode(true);
        LoginData loginData = new LoginData(email, password);
        LoginTask task = new LoginTask(this);
        task.execute(loginData);
    }

    public void onLoginResponse(LoginResponse response) {
        mActivity.loadingMode(false);
        if (response == null) {
            mActivity.onLoginFail(R.string.error_default);
        } else {
            if (response.getUserData() == null) {
                mActivity.onLoginFail(response.getMessage());
            } else {
                UserData userData = response.getUserData();
                mPrefs.setLoggedIn(true);
                mPrefs.setUserName(userData.getName());
                mPrefs.setToken(userData.getToken());
                mActivity.onLoginSuccess(userData.getName());
            }
        }
    }

    private boolean validate(String email, String password) {
        int code = Validator.validateEmail(email);
        switch (code) {
            case Validator.EMPTY_OR_NULL:
                mActivity.showEmailError(R.string.error_required_field);
                return false;
            case Validator.INVALID:
                mActivity.showEmailError(R.string.error_invalid_email);
                return false;
        }

        code = Validator.validatePassword(password);
        switch (code) {
            case Validator.EMPTY_OR_NULL:
                mActivity.showPasswordError(R.string.error_required_field);
                return false;
        }

        return true;
    }

    /**
     * Email and password holder.
     */
    private static class LoginData {
        private final String email;
        private final String password;

        public LoginData(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }

    /**
     * Asynchronous task to call service.
     */
    public static class LoginTask extends AsyncTask<LoginData, Void, LoginResponse> {

        private LoginPresenter mPresenter;

        public LoginTask(@NonNull LoginPresenter presenter) {
            mPresenter = presenter;
        }

        @Override
        protected LoginResponse doInBackground(LoginData... params) {
            if (params == null || params[0] == null) {
                return null;
            }

            LoginData login = params[0];
            MockService service = new MockService();
            return service.requestLogin(login.email, login.password);
        }

        @Override
        protected void onPostExecute(LoginResponse response) {
            mPresenter.onLoginResponse(response);
        }
    }
}
