package com.ciandt.challengeandroid.worldwondersapp.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Shared preferences util.
 */

public class SharePrefs {

    private static final String SHARED_PREFERENCES = "App.SHARED_PREFERENCES";

    private static final String LOGGED_IN = "LOGGED_IN";

    private static final String TOKEN = "TOKEN";

    private static final String USER_NAME = "USER_NAME";

    private static SharePrefs instance;

    private SharedPreferences mPreferences;

    /**
     * Singleton.
     *
     * @param context Application context.
     */
    private SharePrefs(Context context) {
        mPreferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    /**
     * Gets SharePrefs instance.
     *
     * @param context Application context.
     * @return SharePrefs instance.
     */
    public static SharePrefs getInstance(Context context) {
        if (instance == null) {
            instance = new SharePrefs(context);
        }

        return instance;
    }

    /**
     * Check if there is a logged in section.
     */
    public boolean isLoggedIn() {
        return mPreferences.getBoolean(LOGGED_IN, false);
    }

    /**
     * Sets the state of login section.
     */
    public void setLoggedIn(boolean loggedIn) {
        mPreferences.edit().putBoolean(LOGGED_IN, loggedIn).apply();
        if (!loggedIn) {
            setToken(null);
            setUserName(null);
        }
    }

    /**
     * Gets saved token.
     *
     * @return Auth token.
     */
    public String getToken() {
        return mPreferences.getString(TOKEN, null);
    }

    /**
     * Save token.
     *
     * @param token Auth token;
     */
    public void setToken(String token) {
        mPreferences.edit().putString(TOKEN, token).apply();
    }

    /**
     * Gets saved user name.
     *
     * @return User name.
     */
    public String getUserName() {
        return mPreferences.getString(USER_NAME, null);
    }

    /**
     * Save user name.
     *
     * @param userName User name;
     */
    public void setUserName(String userName) {
        mPreferences.edit().putString(USER_NAME, userName).apply();
    }

}
