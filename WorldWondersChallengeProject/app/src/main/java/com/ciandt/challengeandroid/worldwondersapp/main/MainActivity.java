package com.ciandt.challengeandroid.worldwondersapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.login.LoginActivity;
import com.ciandt.challengeandroid.worldwondersapp.model.Wonder;
import com.ciandt.challengeandroid.worldwondersapp.view.WondersListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Main activity, wonders list.
 */
public class MainActivity extends ActionBarActivity {

    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.progress)
    View progress;

    private MainPresenter mPresenter;

    private WondersListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        list.setHasFixedSize(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new WondersListAdapter();
        list.setAdapter(mAdapter);

        mPresenter = new MainPresenter(this);
        mPresenter.initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            mPresenter.onLogoutAction();
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadingMode(boolean enabled) {
        progress.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
    }

    public void onListLoaded(List<Wonder> wonders) {
        mAdapter.setWonders(wonders);
    }

    public void onLogout() {
        finish();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


}
