package com.ciandt.challengeandroid.worldwondersapp.service;

import com.ciandt.challengeandroid.worldwondersapp.model.LoginResponse;
import com.ciandt.challengeandroid.worldwondersapp.model.ModelParser;
import com.ciandt.challengeandroid.worldwondersapp.model.Wonder;

import java.util.ArrayList;
import java.util.List;

/**
 * Mock service.
 */
public class MockService {

    private static final String TAG = "MockService";

    private static final String LOGIN_URL =
            "https://private-anon-3459bde295-worldwondersapi.apiary-mock"
                    + ".com/api/v1/login?email=%s&senha=%s";

    private static final String GET_LIST_URL =
            "https://private-anon-3459bde295-worldwondersapi.apiary-mock.com/api/v1/worldwonders";

    private final ModelParser mParser = new ModelParser();

    public LoginResponse requestLogin(String email, String password) {
        LoginResponse loginResponse = null;
        String url = String.format(LOGIN_URL, email, password);

        String response = NetworkModule.httpRequest(url);
        if (response != null) {
            loginResponse = mParser.parseLoginResponse(response);
        }

        return loginResponse;
    }


    public List<Wonder> getWondersList() {
        List<Wonder> list = new ArrayList<>();

        String response = NetworkModule.httpRequest(GET_LIST_URL);
        if (response != null) {
            list = mParser.parseWondersList(response);
        }

        return list;
    }
}
