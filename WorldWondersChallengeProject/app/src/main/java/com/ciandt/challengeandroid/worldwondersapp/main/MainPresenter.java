package com.ciandt.challengeandroid.worldwondersapp.main;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.ciandt.challengeandroid.worldwondersapp.model.Wonder;
import com.ciandt.challengeandroid.worldwondersapp.service.MockService;
import com.ciandt.challengeandroid.worldwondersapp.util.SharePrefs;

import java.util.List;

/**
 * Main presenter.
 */
public class MainPresenter {

    private MainActivity mActivity;

    private SharePrefs mPrefs;

    public MainPresenter(@NonNull MainActivity activity) {
        mActivity = activity;
        mPrefs = SharePrefs.getInstance(activity);
    }

    public void initialize() {
        mActivity.loadingMode(true);
        LoadWondersTask task = new LoadWondersTask(this);
        task.execute();
    }

    public void onLogoutAction() {
        mPrefs.setLoggedIn(false);
        mActivity.onLogout();
    }

    private void onFinishLoad(List<Wonder> wonders) {
        mActivity.loadingMode(false);
        if (wonders != null) {
            mActivity.onListLoaded(wonders);
        }
    }

    /**
     * Asynchronous task to call service.
     */
    public static class LoadWondersTask extends AsyncTask<Void, Void, List<Wonder>> {

        private MainPresenter mPresenter;

        public LoadWondersTask(@NonNull MainPresenter presenter) {
            mPresenter = presenter;
        }

        @Override
        protected List<Wonder> doInBackground(Void... params) {
            MockService service = new MockService();
            return service.getWondersList();
        }

        @Override
        protected void onPostExecute(List<Wonder> wonders) {
            mPresenter.onFinishLoad(wonders);
        }

        @Override
        protected void onCancelled() {
            mPresenter.onFinishLoad(null);
        }
    }
}
