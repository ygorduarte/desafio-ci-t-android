package com.ciandt.challengeandroid.worldwondersapp.service;

import android.util.Log;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Class responsible to http requests.
 */
public class NetworkModule {

    private static final String TAG = "NetworkModule";

    private static final OkHttpClient client = new OkHttpClient();

    public static String httpRequest(String url) {
        String result = null;
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            if (response != null && response.body() != null) {
                result = response.body().string();
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return result;
    }
}
