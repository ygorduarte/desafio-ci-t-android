package com.ciandt.challengeandroid.worldwondersapp.model;

/**
 * User data.
 */
public class UserData {

    private final String mToken;
    private final String mName;
    private final boolean mEnabled;

    public UserData(String token, String name, boolean enabled) {
        mToken = token;
        mName = name;
        mEnabled = enabled;
    }

    public String getToken() {
        return mToken;
    }

    public String getName() {
        return mName;
    }

    public boolean isEnabled() {
        return mEnabled;
    }
}
