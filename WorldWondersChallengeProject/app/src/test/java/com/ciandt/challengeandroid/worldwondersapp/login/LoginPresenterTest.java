package com.ciandt.challengeandroid.worldwondersapp.login;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.model.LoginResponse;
import com.ciandt.challengeandroid.worldwondersapp.model.UserData;
import com.ciandt.challengeandroid.worldwondersapp.service.MockService;
import com.ciandt.challengeandroid.worldwondersapp.util.SharePrefs;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * LoginPresenter Unit Test.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(SharePrefs.class)
public class LoginPresenterTest {

    @Mock
    private LoginActivity view;

    @Mock
    private MockService service;

    @Mock
    private SharePrefs prefs;

    private LoginPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(SharePrefs.class);
        when(SharePrefs.getInstance(view)).thenReturn(prefs);
        presenter = new LoginPresenter(view);
    }

    @Test
    public void onLoginSave() throws Exception {
        when(prefs.isLoggedIn()).thenReturn(true);
        presenter.initialize();
        verify(view).onLoginSuccess(anyString());
    }

    @Test
    public void onLoginResponseNull() {
        presenter.onLoginResponse(null);
        verify(view).onLoginFail(R.string.error_default);
    }

    @Test
    public void onLoginResponseFail() {
        String message = "Error";
        LoginResponse response = new LoginResponse(message, null);
        presenter.onLoginResponse(response);
        verify(view).onLoginFail(message);
    }

    @Test
    public void onLoginResponseSuccess() {
        String name = "User";
        UserData data = new UserData("token", name, true);
        LoginResponse response = new LoginResponse("Success", data);
        presenter.onLoginResponse(response);
        verify(view).onLoginSuccess(name);
    }

}
