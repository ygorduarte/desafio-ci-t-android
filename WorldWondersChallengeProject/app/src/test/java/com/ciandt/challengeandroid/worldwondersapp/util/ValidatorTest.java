package com.ciandt.challengeandroid.worldwondersapp.util;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

/**
 * Validator Unit Test.
 */
public class ValidatorTest {

    @Test
    public void emptyEmail() {
        int result = Validator.validateEmail("");
        assertEquals(Validator.EMPTY_OR_NULL, result);
    }

    @Test
    public void nullEmail() {
        int result = Validator.validateEmail(null);
        assertEquals(Validator.EMPTY_OR_NULL, result);
    }

    @Test
    public void invalidEmail() {
        int result = Validator.validateEmail("invali.email");
        assertEquals(Validator.INVALID, result);
    }

    @Test
    public void validEmail() {
        int result = Validator.validateEmail("test@gmail.com");
        assertEquals(Validator.VALID, result);
    }

    @Test
    public void emptyPassword() {
        int result = Validator.validatePassword("");
        assertEquals(Validator.EMPTY_OR_NULL, result);
    }

    @Test
    public void nullPassword() {
        int result = Validator.validatePassword(null);
        assertEquals(Validator.EMPTY_OR_NULL, result);
    }

    @Test
    public void validPassword() {
        int result = Validator.validatePassword("password");
        assertEquals(Validator.VALID, result);
    }
}
