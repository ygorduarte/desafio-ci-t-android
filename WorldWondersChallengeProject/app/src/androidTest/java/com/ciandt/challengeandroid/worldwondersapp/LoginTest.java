package com.ciandt.challengeandroid.worldwondersapp;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

import static org.mockito.Mockito.when;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ciandt.challengeandroid.worldwondersapp.login.LoginActivity;
import com.ciandt.challengeandroid.worldwondersapp.service.MockService;
import com.ciandt.challengeandroid.worldwondersapp.service.NetworkModule;
import com.ciandt.challengeandroid.worldwondersapp.util.SharePrefs;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

/**
 * Login screen UI test.
 */
@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityRule = new ActivityTestRule<>(
            LoginActivity.class, false, true);

    @Before
    public void clearLogin() {
        SharePrefs prefs = SharePrefs.getInstance(activityRule.getActivity());
        prefs.setLoggedIn(false);
        activityRule.launchActivity(new Intent());
    }

    @Test
    public void allEmptyEmail() {
        onView(withId(R.id.btn_login)).perform(click());
        String error = activityRule.getActivity().getString(R.string.error_required_field);
        onView(withId(R.id.txt_email)).check(matches(hasErrorText(error)));
    }

    @Test
    public void allInvalidEmail() {
        onView(withId(R.id.txt_email)).perform(typeText("invalid"), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());
        String error = activityRule.getActivity().getString(R.string.error_invalid_email);
        onView(withId(R.id.txt_email)).check(matches(hasErrorText(error)));
    }

    @Test
    public void allEmptyPassword() {
        onView(withId(R.id.txt_email)).perform(typeText("test@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());
        String error = activityRule.getActivity().getString(R.string.error_required_field);
        onView(withId(R.id.txt_password)).check(matches(hasErrorText(error)));
    }

    @Test
    public void invalidCredentials() {
        String email = "test@gmail.com";
        String pass = "password";

        onView(withId(R.id.txt_email)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.txt_password)).perform(typeText(pass), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());
        //Check still in login
        onView(withId(R.id.btn_login)).check(matches(isDisplayed()));
    }


    @Test
    public void successLogin() {
        String email = "t@t.com";
        String pass = "1234";

        onView(withId(R.id.txt_email)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.txt_password)).perform(typeText(pass), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());
        //Check still in login
        onView(withText(R.string.app_name)).check(matches(isDisplayed()));
    }

}
